SHELL := /bin/bash
include .env

# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help build shell
.DEFAULT_GOAL := help


ARCH := $(shell arch)
IMAGE := $(REPO)-$(ARCH)

test:
	echo ${IMAGE}

build:
	docker build . -t ${IMAGE}-base:${VERSION} --target base
	docker build . -t ${IMAGE}-build:${VERSION} --target build
	docker build . -t ${IMAGE}-run:${VERSION} --target run
	docker build . -t ${IMAGE}-test:${VERSION} --target test
	docker build . -t ${IMAGE}-dev:${VERSION} --target dev

shell:
	docker run -it --rm ${IMAGE}-dev:${VERSION} /bin/bash

publish: build
#	docker login cr.grahsl.net
	docker push ${IMAGE}-base:${VERSION} 
	docker push ${IMAGE}-build:${VERSION}
	docker push ${IMAGE}-dev:${VERSION} 
	docker push ${IMAGE}-run:${VERSION} 
	docker push ${IMAGE}-test:${VERSION} 
