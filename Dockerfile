FROM rust:1.65.0-slim-bullseye as base

FROM base as build

FROM base as run

FROM base as test

FROM build as dev

RUN apt-get update

RUN apt-get install -y \
	git vim

