# Build environment for rust projects

This repo is used to build docker images that serve as a build environment for projects which use the rust toolchain.

Includes:

- rust-1.43

## Structure

Several images are built:
- `rust-base`
- `rust-build`
- `rust-dev`

The `-build` variant includes dependencies used only during building (e.g. the `*-dev` debian packages) and is based on `-base`.

The `-dev` variant includes cli tools for testing and development and is based on `-build`.

## Usage

The images can be built:

`make build`

Peek into the `-dev` variant with:

`make shell`

The image cannot be published currently, so they are only available locally.

## Gitlab Runner

Configure your gitlab docker runner with `gitlab-runner register` and specify the `rust-build:0.1.0-master` docker image.

*Note*: Add the `pull_policy = "if-not-present"` to your gitlab-runner `/etc/gitlab-runner/config.toml` under the respective `[runners.docker]` section after registering the runner. This is necessary if you want to use an image that is only available locally (that were built with `make build`). It may be necessary to restart the gitlab-runner.